# Critical CSS Generator
>This is the critical mass<br>So far so good, I heard the other say

_— Aerosmith, 1977_

Speed is everything. This tool makes a small dent in the performance universe by generating required CSS for rendering elements above the fold, such as the header.

## Install
```npm install -g @we-make-websites/critical-css-generator```

## Usage

1. Run the tool

```npx @we-make-websites/critical-css-generator```

2. Follow the steps

You'll be asked for the URL to your site, if you'd like to specify a container (Uses all above the fold elements by default) and that's it.

3. Use your CSS

Simply include the generated CSS in your site for a blazing fast website (unfounded).