#! /usr/bin/env node
const inquirer = require('inquirer');
const mqpacker = require('css-mqpacker');
const postcss = require('postcss');
const puppeteer = require('puppeteer');
const fs = require('fs');

/**
 * Request the page info.
 */
inquirer.prompt([
  {
    type: 'input',
    name: 'url',
    message: 'Define the URL for your page:',
    validate: (value) => {
      const pass = value.match(
        /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
      );

      if (pass) {
        return true;
      }

      return 'Please enter a valid URL.';
    },
  },
  {
    type: 'confirm',
    name: 'enable_target',
    message: 'Would you like to target a specific container?',
    suffix: '\nDefaults to above the fold elements.',
    default: false,
  },
  {
    type: 'input',
    name: 'target_selector',
    message: 'Enter a CSS selector for your target:',
    when: (results) => results.enable_target === true,
    validate: (value) => {
      const pass = value.match(/^[.|#|:|[|a-z].+/);

      if (pass) {
        return true;
      }

      return 'Please enter a valid CSS selector.';
    },
  }
])
  .then((results) => {
    if (results.enable_target) {
      return checkPage(results.url, results.target_selector);
    }

    checkPage(results.url);
  });

/**
 * Generates critical styles.
 * @param {string} url - The page URL.
 * @param {string} selector - Optional target selector.
 */
async function checkPage(url, selector = false) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  
  /**
   * Set up the page.
   */
  await page.setViewport({
    width: 1440,
    height: 798,
  });

  await page.goto(url);

  /**
   * Generate the critical styles.
   */
  const styles = await page.evaluate((selector) => {
    let styles = '';
    let rules = [];
    const target = selector ? document.querySelector(selector) : document;
    const elements = target.querySelectorAll('*');
    const sheets = Array.from(document.styleSheets);

    /**
     * Returns true if the element is in the viewport.
     * @param {HTMLNodeElement} element - The element.
     */
    function isInViewport(element) {
      const bounding = element.getBoundingClientRect();
      
      return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight) &&
        bounding.right <= (window.innerWidth)
      );
    }

    /**
     * Get the critical style rules.
     * - Finds all elements visible in the viewport.
     * - Gets style rules for all elements in each stylesheet.
     */
    sheets.forEach((sheet) => {
      try {
        Array.from(sheet.cssRules).forEach((rule) => {
          if (rule.type === 4) {
            Array.from(rule.cssRules).forEach((mediaRule) => {
              rules.push({
                selectorText: mediaRule.selectorText,
                cssText: `@media ${rule.conditionText} { ${mediaRule.cssText} }`,
              });
            });

            return;
          }

          rules.push(rule);
        });
      } catch (error) {
        return error;
      }
    });

    /**
     * Loops through each element.
     * - Checks if the element is in the viewport.
     * - Appends the style rules to the string.
     */
    elements.forEach((element) => {
      if (!selector) {
        if (!isInViewport(element)) {
          return;
        }
      }

      rules.forEach((rule) => {
        if (styles.includes(rule.cssText)) {
          return;
        }

        if (element.matches(rule.selectorText)) {
          styles += rule.cssText;
        }
      });
    });

    return styles;
  }, selector);

  /**
   * Parse the styles through PostCSS.
   * - Creates a stylesheet.
   * - Writes the rules to it.
   */
  postcss([
    mqpacker,
  ]).process(styles)
    .then((result) => {
      fs.writeFile('styles.css', result.css, (error) => {
        return error;
      });
    });

  /**
   * Closes the browser instance.
   */
  await browser.close();
};